//WAP to find the distance between two point using 4 functions.

#include <stdio.h>
#include <math.h>

int input()
{
    int x,y;
    printf("enter the abscissa and ordinate of  the point:\n");
    scanf("%d %d", &x,&y);
    return x,y;
    
}
float find_distance(int p1,int p2)
{
    int x1,y1,x2,y2;
    p1=(x1,y1);
    p2=(x2,y2);
    float distance;
    distance=sqrt((pow((x1-x2),2))+(pow((y1-y2),2)));
    return distance;
}
void output(int x,int y,float distance)
{
    int x1,y1,x2,y2;
    printf("the distance between (%d,%d) and (%d,%d) is %f", x1,y1,x2,y2,distance);
}
int main()
{
    int a,b;
    float c;
    a=input();
    b=input();
    c=find_distance(a,b);
    output(a,b,c);
    return 0;
}
