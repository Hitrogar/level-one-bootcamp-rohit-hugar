//Write a program to find the volume of a tromboloid using one function

#include <stdio.h>

float volume(float h,float b,float d)
{
    float v;
    v=((h*d)+d)/(3*b);
    return v;
}
int main()
{
    float h,b,d,result=0.0;
    printf("enter the height of tromboloid:\n");
    scanf("%f", &h);
    printf("enter the depth of tromboloid:\n");
    scanf("%f", &d);
    printf("enter the breadth of tromboloid:\n");
    scanf("%f", &b);
    result=volume(h,b,d);
    printf("the volume of tromboloid is %f\n", result);
    return 0;
}