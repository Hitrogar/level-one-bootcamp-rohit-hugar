//Write a program to add two user input numbers using one function.

#include <stdio.h>

float add(float a,float b)
{
    float s=0.0;
	s=a+b;
	return s;
}
int main()
{
    float a,b,sum=0.0;
	printf("enter two numbers:");
	scanf("%f %f", &a,&b);
	sum = add(a,b);
	printf("the sum of two numbers is %f\n", sum);
	return 0;
}	
	